package arlon.portfolio.travelbuddy.viewmodel;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import arlon.portfolio.travelbuddy.service.model.TravelPlace;
import arlon.portfolio.travelbuddy.service.repository.TravelRepository;

public class PlaceListViewModel extends ViewModel {

    private final LiveData<List<TravelPlace>> mPlaceListObservable;

    public PlaceListViewModel() {
        super();

        mPlaceListObservable = TravelRepository.getDestinations("");
    }

    public LiveData<List<TravelPlace>> getPlaceListObservable() {
        return mPlaceListObservable;
    }
}
package arlon.portfolio.travelbuddy.service.model;

public class TravelPlace {

    private String mName = "";
    private String mCityId = "";
    private String mImgUrl = "";
    private String mDesc = "";
    private double mLng = 0.0;
    private double mLat = 0.0;

    public TravelPlace() {

    }

    public void setCityId(String id) {
        mCityId = id;
    }

    public String getCityId() {
        return mCityId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setImgUrl(String imgUrl) {
        mImgUrl = imgUrl;
    }

    public String getImgUrl() {
        return mImgUrl;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setLng(double lng) {
        mLng = lng;
    }

    public double getLng() {
        return mLng;
    }

    public void setLat(double lat) {
        mLat = lat;
    }

    public double getLat() {
        return mLat;
    }
}
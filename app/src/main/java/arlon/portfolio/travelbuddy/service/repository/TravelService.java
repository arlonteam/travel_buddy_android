package arlon.portfolio.travelbuddy.service.repository;

import java.util.List;
import arlon.portfolio.travelbuddy.service.model.TravelParty;
import arlon.portfolio.travelbuddy.service.model.TravelPlace;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TravelService {

    @GET("/destinations")
    Call<List<TravelPlace>> getTravelDestinations(@Query("cityId") String cityId);

    @GET("/parties")
    Call<List<TravelParty>> getTravelParties();
}
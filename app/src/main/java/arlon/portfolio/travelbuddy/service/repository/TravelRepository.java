package arlon.portfolio.travelbuddy.service.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import arlon.portfolio.travelbuddy.service.model.TravelCity;
import arlon.portfolio.travelbuddy.service.model.TravelPlace;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TravelRepository {

    public static LiveData<List<TravelCity>> getCities() {
        return null;
    }

    public static LiveData<List<TravelPlace>> getDestinations(String cityId) {
        final MutableLiveData<List<TravelPlace>> travelDestinations = new MutableLiveData<>();

        RetrofitClient.getClient(null).create(TravelService.class)
            .getTravelDestinations(cityId).enqueue(new Callback<List<TravelPlace>>() {

                @Override
                public void onResponse(Call<List<TravelPlace>> call, Response<List<TravelPlace>> response) {
                    travelDestinations.setValue(response.body());
                }

                @Override
                public void onFailure(Call<List<TravelPlace>> call, Throwable t) {

                }
            }
        );

        return travelDestinations;
    }
}
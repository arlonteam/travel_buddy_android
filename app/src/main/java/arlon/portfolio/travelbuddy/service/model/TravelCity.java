package arlon.portfolio.travelbuddy.service.model;

public class TravelCity {

    private String mId = "";
    private String mName = "";
    private String mCountryName = "";

    public TravelCity() {

    }

    public void setId(String id) {
        mId = id;
    }

    public String getId() {
        return mId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setCountryName(String countryName) {
        mCountryName = countryName;
    }

    public String getCountryName() {
        return mCountryName;
    }
}
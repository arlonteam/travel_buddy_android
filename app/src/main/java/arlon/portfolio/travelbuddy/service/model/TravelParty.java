package arlon.portfolio.travelbuddy.service.model;

public class TravelParty {

    private String mName = "";
    private String mPartyId = "";
    private String mImgUrl = "";
    private String mDesc = "";
    private double mLng = 0.0;
    private double mLat = 0.0;

    public TravelParty() {

    }

    public void setCityId(String id) {
        mPartyId = id;
    }

    public String getCityId() {
        return mPartyId;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setImgUrl(String imgUrl) {
        mImgUrl = imgUrl;
    }

    public String getImgUrl() {
        return mImgUrl;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setLng(double lng) {
        mLng = lng;
    }

    public double getLng() {
        return mLng;
    }

    public void setLat(double lat) {
        mLat = lat;
    }

    public double getLat() {
        return mLat;
    }
}
package arlon.portfolio.travelbuddy;

import android.app.Application;

import com.google.android.libraries.places.api.Places;

import java.util.ArrayList;

import arlon.portfolio.travelbuddy.service.model.TravelCity;
import arlon.portfolio.travelbuddy.service.model.TravelPlace;

public class TravelBuddy extends Application {

    public static ArrayList<TravelCity> CITIES = new ArrayList<>();
    public static ArrayList<TravelPlace> PLACES = new ArrayList<>();

    public void onCreate() {
        super.onCreate();
        initPlaceSDK();
        initPlaces();
    }

    private void initPlaces() {
        loadRome();
        loadParis();
        loadBeijing();
        loadCapeTown();
        loadNewYork();
        loadCairo();
        loadMoscow();
        loadRio();
        loadSydney();
        loadBudapest();
        loadToronto();
        loadDelhi();
    }

    private void loadRome() {
        String cityId = "roma_8723";
        String cityName = "Roma";
        String countryName = "Italy";

        String[] names = {
            "Colosseum", "St. Peter's Basilica", "Trevi Fountain"
        };

        String[] imgUrls = {"colosseum.jpeg", "st_peter_basilica.jpeg", "trevi_fountain.jpeg"};

        String[] descs = {
            "Iconic ancient Roman gladiatorial arena", "World's largest basilica of Christianity",
            "Iconic 18th-century sculpted fountain"
        };

        double[] lats = {41.8902, 41.9022, 41.9009};
        double[] lngs = {12.4922, 12.4539, 12.4833};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadParis() {
        String cityId = "paris_6914";
        String cityName = "Paris";
        String countryName = "France";

        String[] names = {
            "Eiffel Tower", "Louvre Museum", "Palace of Versailles"
        };

        String[] imgUrls = {"eiffel_tower.jpeg", "louvre_museum.jpeg", "palace_of_versailles.jpeg"};

        String[] descs = {
            "Landmark 324m-high 19th-century tower", "Landmark art museum with vast collection",
            "Louis XIV's gilded palace & gardens"
        };

        double[] lats = {48.8566, 48.8606, 48.8049};
        double[] lngs = {2.3522, 2.3376, 2.1204};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadBeijing() {
        String cityId = "beijing_2846";
        String cityName = "Beijing";
        String countryName = "China";

        String[] names = {
            "Great Wall of China", "Forbidden City", "Summer Palace"
        };

        String[] imgUrls = {"great_wall_of_china.jpeg", "forbidden_city.jpeg", "summer_palace.jpeg"};

        String[] descs = {
            "Historic structure winding across China", "Palace, architecture, museum, and historic site",
            "Iconic lakeside retreat for royalty"
        };

        double[] lats = {40.4319, 39.9169, 40.0000};
        double[] lngs = {116.5704, 116.3907, 116.2755};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadCapeTown() {
        String cityId = "cape_town_20746";
        String cityName = "Cape Town";
        String countryName = "South Africa";

        String[] names = {
            "Table Mountain", "Cape of Good Hope", "Kirstenbosch National Botanical Garden",
            "Robben Island", "Boulders Beach"
        };

        String[] imgUrls = {
            "table_mountain.jpeg", "cape_of_good_hope.jpeg",
            "kirstenbosch_national_botanical_garden.jpeg", "robben_island.jpeg",
            "boulders_beach.jpeg"
        };

        String[] descs = {
            "Mountain for hiking, climbing & biking", "Scenic spot & tip of the Cape Peninsula",
            "Nature reserve with mountain views", "Former prison site with a museum",
            "Sandy cove with resident penguin colony"
        };

        double[] lats = {-33.9628, -34.3568, -33.9875, -33.8076, -34.1972};
        double[] lngs = {18.4098, 18.4740, 18.4327, 18.3712, 18.4513};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadNewYork() {
        String cityId = "new_york_6138";
        String cityName = "New York";
        String countryName = "USA";

        String[] names = {
            "Statue of Liberty National Monument", "Central Park", "One World Trade Center"
        };

        String[] imgUrls = {
            "statue_of_liberty_national_monument.jpeg", "central_park.jpeg",
            "one_world_trade_center.jpeg"
        };

        String[] descs = {
            "American icon in New York Harbor", "Urban oasis with ballfields & a zoo",
            "Dining & drinks on the 101st floor"
        };

        double[] lats = {40.6892, 40.7829, 40.7127};
        double[] lngs = {-74.0445, -73.9654, -74.0134};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadCairo() {
        String cityId = "cairo_9250";
        String cityName = "Cairo";
        String countryName = "Egypt";

        String[] names = {
            "Giza Necropolis", "Pyramid of Djoser", "Cairo Citadel"
        };

        String[] imgUrls = {
            "giza_necropolis.jpeg", "central_park.jpeg", "cairo_citadel.jpeg"
        };

        String[] descs = {
            "Site of the Great Pyramids & the Sphinx", "Ruins of step pyramid built for pharaoh",
            "Landmark medieval Islamic fortress"
        };

        double[] lats = {29.9773, 29.8713, 30.0299};
        double[] lngs = {31.1325, 31.2165, 31.2611};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadMoscow() {
        String cityId = "moscow_1753";
        String cityName = "Moscow";
        String countryName = "Russia";

        String[] names = {
            "The Moscow Kremlin", "Red Square", "St. Basil's Cathedral"
        };

        String[] imgUrls = {
            "the_moscow_kremlin.jpeg", "red_square.jpeg", "st_basil's_cathedral.jpeg"
        };

        String[] descs = {
            "Monumental architectural complex", "Landmark square & site of cathedral",
            "Museum in iconic former Orthodox church"
        };

        double[] lats = {55.7520, 55.7539, 55.7525};
        double[] lngs = {37.6175, 37.6208, 37.6231};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadRio() {
        String cityId = "rio_de_janeiro_6381";
        String cityName = "Rio de Janeiro";
        String countryName = "Brazil";

        String[] names = {
            "Christ the Redeemer", "Tijuca National Park"
        };

        String[] imgUrls = {
            "christ_the_redeemer.jpeg", "tijuca_national_park.jpeg"
        };

        String[] descs = {
            "Iconic Christ statue atop a mountain", "Rio's rainforest, with famed views"
        };

        double[] lats = {55.7520, 55.7539, 55.7525};
        double[] lngs = {37.6175, 37.6208, 37.6231};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadSydney() {
        String cityId = "sydney_3124";
        String cityName = "Sydney";
        String countryName = "Australia";

        String[] names = {
            "Sydney Opera House", "Sydney Harbour Bridge", "Royal Botanic Gardens"
        };

        String[] imgUrls = {
            "sydney_opera_house.jpeg", "sydney_harbour_bridge.jpeg", "royal_botanic_gardens.jpeg"
        };

        String[] descs = {
            "Australia's iconic performing-arts venue", "Massive steel arched bridge with lookout",
            "Oasislike gardens with glasshouses"
        };

        double[] lats = {-33.8568, -33.8523, -33.8642};
        double[] lngs = {151.2153, 151.2108, 151.2166};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadBudapest() {
        String cityId = "budapest_91063";
        String cityName = "Budapest";
        String countryName = "Hungary";

        String[] names = {
            "Buda Castle", "Heroes' Square", "Széchenyi Thermal Bath"
        };

        String[] imgUrls = {
            "buda_castle.jpeg", "heroes_square.jpeg", "széchenyi_thermal_bath.jpeg"
        };

        String[] descs = {
            "Castle home of Hungarian art collection", "Major square with landmark statues",
            "Grand spa with in- & outdoor pools"
        };

        double[] lats = {47.4962, 47.5149, 47.5152};
        double[] lngs = {19.0396, 19.0779, 19.0830};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadToronto() {
        String cityId = "toronto_1639";
        String cityName = "Toronto";
        String countryName = "Canada";

        String[] names = {
            "Royal Ontario Museum", "Tommy Thompson Park", "Colonel Samuel Smith Park"
        };

        String[] imgUrls = {
            "royal_ontario_museum.jpeg", "tommy_thompson_park.jpeg", "colonel_samuel_smith_park.jpeg"
        };

        String[] descs = {
            "Huge range of culture & nature exhibits", "Waterfront green spot with nature trails",
            "Bird-watching & an ice-skating trail"
        };

        double[] lats = {43.6677, 43.6522, 43.5914};
        double[] lngs = {-79.3948, -79.3229, -79.5133};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadDelhi() {
        String cityId = "delhi_7827";
        String cityName = "Delhi";
        String countryName = "India";

        String[] names = {
            "Humayun’s Tomb", "Lotus Temple", "India Gate"
        };

        String[] imgUrls = {
            "humayuns_tomb.jpeg", "lotus_temple.jpeg", "india_gate.jpeg"
        };

        String[] descs = {
            "Palatial 16th-century tomb of Humayun", "Temple with a flower-like design",
            "1920s triumphal arch & war memorial"
        };

        double[] lats = {28.5933, 28.5535, 28.6129};
        double[] lngs = {77.2507, 77.2588, 77.2295};

        loadTravelPoints(cityId, cityName, countryName, names, imgUrls, descs, lats, lngs);
    }

    private void loadTravelPoints(String cityId, String cityName, String countryName, String[] names,
                  String[] imgUrls, String[] descs, double[] lats, double[] lngs) {

        TravelCity city = new TravelCity();
        city.setId(cityId);
        city.setName(cityName);
        city.setCountryName(countryName);
        CITIES.add(city);

        TravelPlace point = null;

        for (int i = 0; i < names.length; i++) {
            point = new TravelPlace();
            point.setCityId(cityId);
            point.setName(names[i]);
            point.setImgUrl(imgUrls[i]);
            point.setDesc(descs[i]);
            point.setLat(lats[i]);
            point.setLng(lngs[i]);
            PLACES.add(point);
        }
    }

    private void initPlaceSDK() {
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_api_key));
        }
    }
}
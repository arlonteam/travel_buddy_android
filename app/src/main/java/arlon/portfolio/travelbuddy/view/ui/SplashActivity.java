package arlon.portfolio.travelbuddy.view.ui;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;

import arlon.portfolio.travelbuddy.R;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SplashActivity extends AppCompatActivity {

    private ImageView mIvKnotsHairLogo = null;

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            moveToHomeScreen();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);

        mIvKnotsHairLogo = (ImageView) findViewById(R.id.ivKnotsHairLogo);
//        mIvKnotsHairLogo.setVisibility(View.INVISIBLE);

//        new Handler().postDelayed(mRunnable, 3000);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @OnClick(R.id.ivKnotsHairLogo)
    public void imageClicked() {
        moveToHomeScreen();
    }

    private void moveToHomeScreen() {
        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
        /*intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);*/
        startActivity(intent);
    }
}
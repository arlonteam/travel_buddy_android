package arlon.portfolio.travelbuddy.view.adapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.util.Strings;

import java.util.ArrayList;

import arlon.portfolio.travelbuddy.R;
import arlon.portfolio.travelbuddy.TravelBuddy;
import arlon.portfolio.travelbuddy.service.model.TravelPlace;

public class TravelPlaceAdapter extends RecyclerView.Adapter<TravelPlaceAdapter.PlaceViewHolder> {

    ArrayList<TravelPlace> mTravelPlaces = TravelBuddy.PLACES;

    @NonNull
    @Override
    public PlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView cardView = (CardView) LayoutInflater.from(parent.getContext()).inflate(
            R.layout.layout_place, parent, false);

        return new PlaceViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaceViewHolder holder, int position) {
        holder.update(mTravelPlaces.get(position));
    }

    @Override
    public int getItemCount() {
        return mTravelPlaces.size();
    }

    public Filter getFilter() {
        return new ItemFilter();
    }

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<TravelPlace> travelPlaces = new ArrayList<>(TravelBuddy.PLACES);

            /*if (Strings.isEmptyOrWhitespace(constraint.toString())) {
//                travelPlaces.addAll(TravelBuddy.CITIES);
            } else {
                for (TravelPlace travelPlace : TravelBuddy.PLACES) {
                    if (travelPlace.getName().toLowerCase().contains(constraint.toString()
                            .toLowerCase())) {
                        travelPlaces.add(travelPlace);
                    }
                }
            }*/

            results.values = travelPlaces;
            results.count = travelPlaces.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mTravelPlaces = (ArrayList<TravelPlace>) results.values;
            notifyDataSetChanged();
        }
    }

    public class PlaceViewHolder extends RecyclerView.ViewHolder {

        public PlaceViewHolder(View view) {
            super(view);
        }

        public void update(TravelPlace travelPlace) {
            ((ImageView) itemView.findViewById(R.id.ivPlace)).setImageURI(Uri.parse(travelPlace.getImgUrl()));
            ((TextView) itemView.findViewById(R.id.tvPlace)).setText(travelPlace.getName());
        }
    }
}
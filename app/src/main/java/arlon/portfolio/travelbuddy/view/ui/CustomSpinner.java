package arlon.portfolio.travelbuddy.view.ui;

import android.content.Context;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatSpinner;

public class CustomSpinner extends AppCompatSpinner {

    private OnItemSelectedListener mListener = null;

    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setSelection(int position) {
        super.setSelection(position);

        if (mListener != null) mListener.onItemSelected(null, null, position, 0);
    }

    public void setListener(OnItemSelectedListener listener) {
        mListener = listener;
    }
}
package arlon.portfolio.travelbuddy.view.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.ViewGroup;
import com.google.android.material.tabs.TabLayout;
import java.util.Map;

import arlon.portfolio.travelbuddy.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.tlMenu)
    public TabLayout mTlMenu;

    @Nullable
    @BindView(R.id.vpPages)
    public StaticPager mVpPages;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mVpPages.setAdapter(mSectionsPagerAdapter);
        mVpPages.addOnPageChangeListener(new ViewPagerChangeListener());
        mVpPages.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTlMenu));
        mTlMenu.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mVpPages));
        mVpPages.setCurrentItem(0);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        Map<Integer, Fragment> mFragments = new ArrayMap<>(4);

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (mFragments.get(position) != null) return mFragments.get(position);

            switch (position) {
                case 0:
                    return HomeFragment.newInstance();

                case 1:
                    return FavouriteFragment.newInstance();

                case 2:
                    return MessagesFragment.newInstance();

                case 3:
                    return HomeFragment.newInstance();

                default:
                    throw new IllegalArgumentException("Unsupported tab position: " + position);
            }
        }

        @NonNull
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment createdFragment = (Fragment) super.instantiateItem(
                    container, position);

//            createdFragment.setTabSwitcher(new TabSwitchListener());
            mFragments.put(position, createdFragment);

            return createdFragment;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

        }

        Fragment getChildFragment(int position) {
            return mFragments.get(position);
        }
    }

    private class ViewPagerChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.home_menu, menu);
        return true;
    }

    public void onBackPressed() {
        if (mVpPages.getCurrentItem() != 0) {
            mVpPages.setCurrentItem(0);
            return;
        }

        super.onBackPressed();
    }

    private class TabSwitchListener implements TabSwitcherInterface {

        @Override
        public void goToTab(int tabIndex) {
            mVpPages.setCurrentItem(tabIndex);
        }
    }

    public interface TabSwitcherInterface {

        public void goToTab(int tabIndex);
    }
}
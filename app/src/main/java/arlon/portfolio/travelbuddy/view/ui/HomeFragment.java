package arlon.portfolio.travelbuddy.view.ui;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import arlon.portfolio.travelbuddy.R;
import arlon.portfolio.travelbuddy.databinding.FragmentHomeBinding;
import arlon.portfolio.travelbuddy.view.adapter.TravelCityAdapter;
import arlon.portfolio.travelbuddy.view.adapter.TravelPlaceAdapter;
import arlon.portfolio.travelbuddy.viewmodel.PlaceListViewModel;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;

public class HomeFragment extends Fragment {

    /*@BindView(R.id.rvCities)
    public RecyclerView mRvCities;*/

    private PlaceListViewModel mModel = null;

    private TravelCityAdapter mCityAdapter = null;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /*View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        return view;*/
        FragmentHomeBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home,
            container, false);

        binding.setHandlers(new EditTextHandlers());

        mCityAdapter = new TravelCityAdapter();
        binding.rvCities.setHasFixedSize(true);
        binding.rvCities.setItemAnimator(new DefaultItemAnimator());
        binding.rvCities.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.rvCities.setAdapter(mCityAdapter);

        binding.rvPlaces.setHasFixedSize(true);
        binding.rvPlaces.setItemAnimator(new DefaultItemAnimator());
        binding.rvPlaces.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        binding.rvPlaces.setAdapter(new TravelPlaceAdapter());

        return binding.getRoot();
    }

    public void onViewCreated(View view, Bundle bundle) {
//        initAutoComplete();
        /*mCityAdapter = new TravelCityAdapter();
        mRvCities.setHasFixedSize(true);
        mRvCities.setItemAnimator(new DefaultItemAnimator());
        mRvCities.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvCities.setAdapter(mCityAdapter);*/
        getPredictions("");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mModel = new PlaceListViewModel();
    }

    private void getPredictions(String query) {
        mCityAdapter.getFilter().filter(query);
//        mRvCities.performClick();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnTextChanged(R.id.edtSearch)
    protected void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.d("HOME_FRAGMENT", "feature name:" + s.toString());
    }

    /*private class CityAdapter extends ArrayAdapter<TravelCity> implements Filterable {

        ArrayList<TravelCity> mTravelCities = new ArrayList<>();

        public CityAdapter() {
            super(HomeFragment.this.getContext(), android.R.layout.simple_spinner_item);
        }

        public View getView(int pos, View convertView, ViewGroup parent) {
            return buildView(pos,  convertView, parent);
        }

        public View getDropDownView(int pos, View convertView, ViewGroup parent) {
            return buildView(pos,  convertView, parent);
        }

        private View buildView(int pos, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

                convertView = layoutInflater.inflate(R.layout.layout_city_drop_down, null);
            }

            TravelCity travelCity = getItem(pos);

            ((TextView) convertView).setText(travelCity.getName() + ", " + travelCity.getCountryName());

            return convertView;
        }

        public TravelCity getItem(int position) {
            return mTravelCities.get(position);
        }
    }*/

    public class EditTextHandlers {

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            Log.d("HANDLERS_VIEW", s.toString());
            getPredictions(s.toString());
        }

        public void onFocusChange(View view, boolean hasFocus) {
            if(!hasFocus) {

            }
            Log.d("HANDLERS_VIEW", "FOCUSED: " + hasFocus);
        }
    }
}
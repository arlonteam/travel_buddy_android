package arlon.portfolio.travelbuddy.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.common.util.Strings;
import java.util.ArrayList;
import arlon.portfolio.travelbuddy.R;
import arlon.portfolio.travelbuddy.TravelBuddy;
import arlon.portfolio.travelbuddy.service.model.TravelCity;
import arlon.portfolio.travelbuddy.service.model.TravelPlace;

public class TravelCityAdapter extends RecyclerView.Adapter<TravelCityAdapter.CityViewHolder> {

    ArrayList<TravelCity> mTravelCities = TravelBuddy.CITIES;

    @NonNull
    @Override
    public CityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        TextView textView = (TextView) LayoutInflater.from(parent.getContext()).inflate(
            R.layout.layout_city, parent, false);

        return new CityViewHolder(textView);
    }

    @Override
    public void onBindViewHolder(@NonNull CityViewHolder holder, int position) {
        holder.update(mTravelCities.get(position));
    }

    @Override
    public int getItemCount() {
        return mTravelCities.size();
    }

    public Filter getFilter() {
        return new ItemFilter();
    }

    /*private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<TravelPlace> travelPlaces = new ArrayList<>();

            if (Strings.isEmptyOrWhitespace(constraint.toString())) {
                travelPlaces.addAll(TravelBuddy.PLACES);
            } else {
                for (TravelPlace travelPlace : TravelBuddy.PLACES) {
                    if (travelPlace.getName().toLowerCase().contains(constraint.toString()
                            .toLowerCase())) {
                        travelPlaces.add(travelPlace);
                    }
                }
            }

            results.values = travelPlaces;
            results.count = travelPlaces.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mTravelPlaces = (ArrayList<TravelCity>) results.values;
            notifyDataSetChanged();
        }
    }*/

    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();
            ArrayList<TravelCity> travelPlaces = new ArrayList<>();

            if (Strings.isEmptyOrWhitespace(constraint.toString())) {
//                travelPlaces.addAll(TravelBuddy.CITIES);
            } else {
                for (TravelCity travelCity : TravelBuddy.CITIES) {
                    if (travelCity.getName().toLowerCase().contains(constraint.toString()
                            .toLowerCase())) {
                        travelPlaces.add(travelCity);
                    }
                }
            }

            results.values = travelPlaces;
            results.count = travelPlaces.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mTravelCities = (ArrayList<TravelCity>) results.values;
            notifyDataSetChanged();
        }
    }

    public class CityViewHolder extends RecyclerView.ViewHolder {

        public CityViewHolder(View view) {
            super(view);
        }

        public void update(TravelCity travelCity) {
            ((TextView) itemView.findViewById(R.id.tvCity)).setText(travelCity.getName());
        }
    }
}
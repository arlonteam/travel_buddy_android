package arlon.portfolio.travelbuddy.view.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.viewpager.widget.ViewPager;

public class StaticPager extends ViewPager {

    public StaticPager(Context context) {
        super(context, null);
    }

    public StaticPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setSoundEffectsEnabled(true);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return false;
    }
}